package com.owen.tasknote.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.owen.tasknote.BR;
import com.owen.tasknote.R;
import com.owen.tasknote.data.TaskNoteBindingHolder;
import com.owen.tasknote.data.TaskNoteModel;
import com.owen.tasknote.databinding.ItemNoteBinding;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by owen on 2017/5/12.
 */

public class NoteListAdapter extends RecyclerView.Adapter<TaskNoteBindingHolder> {

    private WeakReference<ArrayList<TaskNoteModel>> mItems;
    private Context mContext;

    public NoteListAdapter(Context context, WeakReference<ArrayList<TaskNoteModel>> items){
        mContext = context;
        mItems = items;
    }

    @Override
    public TaskNoteBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemNoteBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_note, parent, false);
        TaskNoteBindingHolder holder = new TaskNoteBindingHolder(binding.getRoot());
        holder.setBinding(binding);
//        holder.getBinding().checkBox.setOnCheckedChangeListener();
        return holder;
    }

    @Override
    public void onBindViewHolder(TaskNoteBindingHolder holder, int position) {
        if(mItems.get() != null) {
            TaskNoteModel noteModel = mItems.get().get(position);
            holder.getBinding().setVariable(BR.task, noteModel);
            holder.getBinding().executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        if(mItems.get() != null)
            return mItems.get().size();

        return 0;
    }

//    public interface RecyclerViewItemClickListener{
//
//    }
}
