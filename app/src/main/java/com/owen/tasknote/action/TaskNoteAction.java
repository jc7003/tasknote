package com.owen.tasknote.action;

import android.databinding.BindingAdapter;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.owen.tasknote.data.TaskNoteModel;

/**
 * Created by owen on 2017/5/13.
 */

public class TaskNoteAction {

    @BindingAdapter("bind:itemOnCheckedChanged")
    public static void setOnCheckedChanged(final CompoundButton buttonView, final TaskNoteModel viewModel) {
        buttonView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("TaskNoteAction", "onCheckedChanged:" + viewModel.getNote() + ", " + " isChecked:" + isChecked);
            }
        });
    }

    @BindingAdapter("bind:itemClick")
    public static void onItemClick(final ConstraintLayout view, final TaskNoteModel viewModel) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TaskNoteAction", "onClick:" + viewModel.getNote());
            }
        });
    }
}
