package com.owen.tasknote.data;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.owen.tasknote.databinding.ItemNoteBinding;

/**
 * Created by owen on 2017/5/12.
 */

public class TaskNoteBindingHolder extends RecyclerView.ViewHolder{
    private ItemNoteBinding binding;

    public TaskNoteBindingHolder(View itemView) {
        super(itemView);
    }

    public ItemNoteBinding getBinding(){
        return binding;
    }

    public void setBinding(ItemNoteBinding holder){
        binding = holder;
    }
}
