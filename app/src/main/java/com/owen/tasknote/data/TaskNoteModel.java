package com.owen.tasknote.data;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.owen.tasknote.BR;

/**
 * Created by owen on 2017/5/12.
 */

public class TaskNoteModel extends BaseObservable {

    private String mDate;
    private String mNote;
    private String mPriority;
    private boolean mComplete;

    public TaskNoteModel(String date, String note, String priority, boolean complete) {
        mDate = date;
        mNote = note;

        mPriority = priority;
        mComplete = complete;
    }

    @Bindable
    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
        notifyPropertyChanged(BR.date);
    }

    @Bindable
    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
        notifyPropertyChanged(BR.note);
    }

    @Bindable
    public String getPriority() {
        return mPriority;
    }

    public void setPriority(String priority) {
        mPriority = priority;
        notifyPropertyChanged(BR.priority);
    }

    @Bindable
    public boolean isComplete() {
        return mComplete;
    }

    public void setComplete(boolean complete) {
        mComplete = complete;
        notifyPropertyChanged(BR.complete);
    }

//    public static class TaskNoteAction {
//
//        @BindingAdapter(value = {"bind:itemOnCheckedChanged"}, requireAll = false)
//        public static void setOnCheckedChanged(final CompoundButton buttonView, final TaskNoteModel viewModel) {
//            buttonView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    Log.d("TaskNoteAction", "onCheckedChanged:" + viewModel.getNote() + ", " + " isChecked:" + isChecked );
//                }
//            });
//        }
//
//        @BindingAdapter(value = {"bind:itemClick"}, requireAll = false)
//        public static void setOnClick(final View view, final TaskNoteModel viewModel){
//            Log.d("TaskNoteAction", "onClick:" + viewModel.getNote() );
//        }
//    }
}
