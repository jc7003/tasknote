package com.owen.tasknote.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.owen.tasknote.R;
import com.owen.tasknote.adapter.NoteListAdapter;
import com.owen.tasknote.data.TaskNoteModel;
import com.owen.tasknote.databinding.ActivityNoteListBinding;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class NoteListActivity extends AppCompatActivity {

    private ActivityNoteListBinding mBinding;
    private WeakReference<ArrayList<TaskNoteModel>> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_note_list);
        mBinding.toolbar.setTitle(R.string.main_title);
        setSupportActionBar(mBinding.toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mBinding.contentLayout.recyclerView.setHasFixedSize(true);
        mBinding.contentLayout.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.contentLayout.recyclerView.setItemAnimator(new DefaultItemAnimator());

        initData();
    }

    private void initData(){
        ArrayList<TaskNoteModel> list = new ArrayList<>();

        list.add(new TaskNoteModel("2017-05-12", "mvvm開發注意事項", "1", false));
        list.add(new TaskNoteModel("2017-05-12", "筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "1筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "2筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "3筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "4筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "5筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "6筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "7筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "8筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "9筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "0筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "11筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "12筆電", "3", false));
        list.add(new TaskNoteModel("2017-05-12", "13筆電", "3", false));


        mList = new WeakReference<>(list);
        NoteListAdapter adapter = new NoteListAdapter(this, mList);
        mBinding.contentLayout.recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
